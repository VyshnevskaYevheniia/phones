package Phones;

public final class IPhone extends Phone {

    private String udid;

    public IPhone(String name, String IMEI, String screenSize, String screenResolution, String OS, City city, String assignee, String UDID) {
        super(name, IMEI, screenSize, screenResolution, OS, city, assignee);
        this.udid = udid;
        this.setdeviceType("IPhone");

    }

    public void print() {
        String result = this.getdeviceType() + " " + this.getname() +
                "\nOS " + this.getOS() +
                "\nDISPLAY:" + this.getscreenSize() + " inch, " + this.getscreenResolution() +
                "\nIMEI:" + this.getIMEI() +
                "\nUDID:" + this.udid +
                "\nASSIGNED TO:" + (this.getassignee() != null && !this.getassignee().equals("") ? this.getassignee() : "") +
                "\nIS AVAILABLE?:" + this.isAvailable() + "\n";
        System.out.println(result);
    }
}
