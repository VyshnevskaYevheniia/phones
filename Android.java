package Phones;

public class Android extends Phone {
    private boolean isRoot;
    public Android(String name, String IMEI, String screenSize, String screenResolution, String OS, City city, String assignee, boolean isRooted) {
        super(name, IMEI, screenSize, screenResolution, OS, city, assignee);
        this.isRoot = isRoot;
        this.setdeviceType("ANDROID");
    }
    public void print(){
        String result = this.getdeviceType() + " " +  this.getname() +
                "\nOS " +  this.getOS() +
                "\nDISPLAY:" + this.getscreenSize() + " inch, " + this.getscreenResolution() +
                "\nIMEI:" + this.getIMEI() +
                "\nASSIGNED TO:" + (this.getassignee() != null && !this.getassignee().equals("") ? this.getassignee() : "") +
                "\nLOCATED AT:" + this.getcity() +
                (this.isRoot ? "\nROOTED":"\nNOT ROOTED") +
                "\nIS AVAILABLE?:" + this.isAvailable() + "\n";
        System.out.println(result);
    }

}
