package Phones;
import java.util.Comparator;
public enum Sorted implements Comparator<Phone> {

        ByOS{
            @Override
            public int compare(Phone b1, Phone b2) {
                return Double.valueOf(b1.getOS()).compareTo(Double.valueOf(b2.getOS()));
            }
        },
        ByscreenSize{
            @Override
            public int compare(Phone b1, Phone b2) {
                return Double.valueOf(b1.getscreenSize()).compareTo(Double.valueOf(b2.getscreenSize()));
            }
        }

}
