package Phones;

public enum City {

    KYIV,
    LVIV,
    DNIPRO,
    KHARKIV,
    ODESSA
}
