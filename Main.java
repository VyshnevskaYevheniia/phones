package Phones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Phone iPhone10 = new IPhone("Iphone10", "7271", "4.8", "100x300", "16", City.DNIPRO, "me", "91");
        Phone iPhone7 = new IPhone("Iphone7", "7273", "3.8", "100x100", "18", City.ODESSA, "", "92");
        Phone iPhoneSE = new IPhone("IphoneSE", "7274", "5", "150x250", "12", City.LVIV, "", "93");
        Phone iPhone6 = new IPhone("Iphone6", "7275", "3.8", "100x300", "8", City.KYIV, "", "94");
        Phone Xaomi2 = new Android("Xaomi2", "3122", "6.4", "100x300", "2", City.KYIV, "me", true);
        Phone Xaomi1 = new Android("Xaomi1", "3121", "6.2", "100x250", "3", City.LVIV, "HIM", true);
        Phone Xaomi3 = new Android("Xaomi3", "3123", "5", "90x80", "1", City.LVIV, "HIM", false);

        Xaomi2.print();
        iPhone7.vibraten();
        iPhone10.print();
        System.out.println(Xaomi3.equals(Xaomi1));
        System.out.println(Xaomi1.equals(Xaomi2));

        System.out.println("\n=======Device warehouse\n");

        List<Phone> devices = new ArrayList<>();
        devices.add(Xaomi1);
        devices.add(Xaomi2);
        devices.add(Xaomi3);
        devices.add(iPhone6);
        devices.add(iPhoneSE);
        devices.add(iPhone7);
        devices.add(iPhone10);

        System.out.println("\n=======sorted by screen size\n");
        devices.sort(Sorted.ByscreenSize);
        for(Phone phone : devices)
            System.out.println(phone);

        System.out.println("\n=======sorted bydefault\n");
        Collections.sort(devices);
        for(Phone phone : devices)
            phone.print();


        System.out.println("\n=======sorted byOS\n");
        devices.sort(Sorted.ByOS);
        for(Phone phone : devices)
            System.out.println(phone);

        System.out.println(Phone.getCounter());
    }

}
