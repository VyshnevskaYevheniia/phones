package Phones;

public abstract class Phone implements Printable, Vibrate, Available, Comparable<Phone> {

    private String deviceType;
    private String OS;
    private String IMEI;
    private String name;
    private String screenResolution;
    private String screenSize;
    private City city;
    private boolean isAvailable;
    private String assignee;
    private static int counter = 0;

    public Phone(String DeviceType, String OS, String IMEI, String name, String screenResolution, String screenSize, City city, String assignee) {
        this.name = name;
        this.IMEI = IMEI;
        this.deviceType = DeviceType;
        this.OS = OS;
        this.screenResolution = screenResolution;
        this.city = city;
        this.screenSize = screenSize;
        this.assignee = assignee;
        this.isAvailable = isAvailable();
        counter++;
    }

    public Phone(String name, String imei, String screenSize, String screenResolution, String os, City city, String assignee) {
    }

    public static int getCounter() {
        return counter;
    }

    public boolean isAvailable() {
        if (assignee == null)
            return true;
        return (assignee.isBlank());
    }

    public String getIMEI() {
        return IMEI;
    }

    public String getscreenResolution() {
        return screenResolution;
    }

    public String getname() {
        return name;
    }

    public String getscreenSize() {
        return screenSize;
    }


    public String getOS() {
        return OS;
    }

    public String getassignee() {
        return assignee;
    }

    public void setassigneee(String assignee) {
        this.assignee = assignee;
        this.isAvailable = isAvailable();

    }

    public String getdeviceType() {
        return deviceType;
    }

    public String setdeviceType(String iPhone) {
        return deviceType;

    }


    public City getcity() {
        return city;
    }

    public boolean getIsAvailable() {
        return isAvailable;
    }
    @Override
    public boolean equals(Object obj) {
        System.out.print("IS EQUALS? ");
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Phone phone = (Phone) obj;
        return this.getOS().equals(phone.getOS()) &&
                this.getscreenSize().equals(phone.getscreenSize()) &&
                this.getscreenResolution().equals(phone.getscreenResolution());
    }

    @Override
    public int compareTo(Phone phone) {
        int result = this.getdeviceType().compareTo(phone.getdeviceType());
        if (result != 0)
            return result;
        result = this.getname().compareTo(phone.getname());
        if (result != 0)
            return result;
        result = Double.valueOf(this.getOS()).compareTo(Double.valueOf(phone.getOS()));
        return result;
    }

    @Override
    public String toString(){
        return this.getdeviceType() + " " +  this.getname() +
                " OS " +  this.getOS() +
                " DISPLAY:" + this.getscreenSize() + "(" + this.getscreenResolution()+ ")";
    }

}

